# SlothCreator: Building DocC Documentation

Build DocC documentation for a Swift package that contains a DocC Catalog.

## Overview

This sample code project is based on the SlothCreator project created by Apple
associated with the following Apple's WWDC sessions:
- [10244: Create rich documentation with Swift-DocC](https://developer.apple.com/wwdc23/10244) (WWDC23)
- [110368: What's new in Swift-DocC](https://developer.apple.com/wwdc22/110368) (WWDC22)
- [110369: Improve the discoverability of your Swift-DocC content](https://developer.apple.com/wwdc22/110369) (WWDC22)
- [10166: Meet DocC documentation in Xcode](https://developer.apple.com/wwdc21/10166) (WWDC21)
- [10167: Elevate your DocC documentation in Xcode](https://developer.apple.com/wwdc21/10167) (WWDC21)
- [10236: Host and automate your DocC Documentation](https://developer.apple.com/wwdc21/10236) (WWDC21)
- [10235: Build interactive tutorials in DocC](https://developer.apple.com/wwdc21/10235) (WWDC21)

The purpose of this project is to explore possibilities of integrating swift-docc with Gitlab.

Documentation generated from this repository is available
[here](https://mz82kb.gitlab.io/slothcreator/tutorials/slothcreator/)
